import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AppState();
}

class AppState extends State<MyApp> {
  Position position;
  List<Placemark> placemarks;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Geolocator Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Geolocator Demo'),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RaisedButton(child: Text('按拉'), onPressed: () async {
                try {
                  Position p =  await Geolocator.getCurrentPosition();
                   List<Placemark> pm = await placemarkFromCoordinates(22.640907, 120.304465);

                  setState(() {
                    position = p;
                    placemarks = List.from(pm);
                  });
                } catch (e) {
                  print('error: $e');
                }
              }),
              const Padding(padding: EdgeInsets.only(top: 10.0)),
              Text('Latitude: ${position?.latitude ?? 0}'),
              const Padding(padding: EdgeInsets.only(top: 10.0)),
              Text('Longitude: ${position?.longitude ?? 0}'),
              const Padding(padding: EdgeInsets.only(top: 10.0)),
              Text('Address: ${placemarks?.length}')],
          ),
        ),
      ),
    );
  }
}
